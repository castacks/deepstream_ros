#define _GLIBCXX_USE_CXX11_ABI 1
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <gst/gst.h>
#include <stdio.h>
#include <time.h>
#include <timesyncdata.h>

using namespace std;

#define USE(x) ((void)(x))

static GstPipeline *gst_pipeline = NULL;
static string launch_string;

GstClockTime usec = 1000000;
static int w = 2448;
static int h = 2058;

TimeSync::SharedMemoryInterface shminterface;
TimeSync::Data shmdata;

static GQuark gst_buffer_metadata_quark = 0;

typedef struct AuxBufferData {
  gint64 frame_num;
  gint64 timestamp;
} AuxData;

static void convertTimeToTimeSpec(const gint64 input, struct timespec &converted)
{
  converted.tv_sec = input/1000000000;
  gint64 nsecsleft = input - converted.tv_sec * 1000000000;
  converted.tv_nsec = nsecsleft;
}

static GstPadProbeReturn nvargus_src_pad_buffer_probe (GstPad * pad, GstPadProbeInfo * info,   gpointer u_data)
{
  shminterface.getData(shmdata);
  AuxData *meta = NULL;
  if(info)
    { 
      GstBuffer *buf = (GstBuffer *) info->data;
      if(buf)
	{
	  gst_buffer_metadata_quark = g_quark_from_static_string ("GstBufferMetaData");
	  meta = (AuxData *) gst_mini_object_get_qdata (GST_MINI_OBJECT_CAST (buf),gst_buffer_metadata_quark);
	  if(meta)
	    {
	      printf(">>> Gstreamer:Frame #%lu : Timestamp: %lu\n", meta->frame_num, meta->timestamp);
	      struct timespec systemtime,monotonic,monotonicimage,time;
	      clock_gettime( CLOCK_REALTIME, &systemtime);
	      printf("RT %lu\t%lu\n",systemtime.tv_sec,systemtime.tv_nsec);
	      clock_gettime( CLOCK_MONOTONIC, &monotonic);
	      printf("MT %lu\t%lu\n",monotonic.tv_sec,monotonic.tv_nsec);
	      convertTimeToTimeSpec(meta->timestamp,monotonicimage);
	      printf("MTI %lu\t%lu\n",monotonicimage.tv_sec,monotonicimage.tv_nsec);
	      if(convertToSystemTimeMono(monotonicimage,shmdata,time))
		{
		  printf("tc: %ld %ld\n",time.tv_sec,time.tv_nsec);
		  printf("monotonic diff: %ld ns\n",systemtime.tv_nsec - time.tv_nsec);  
		}else
		printf("no init\n");
	      
	    }
	  else
	    printf("meta null\n");
	}
      else
	printf("buf null\n");
      
    }
  else
    printf("info null\n");
   return GST_PAD_PROBE_OK;
}





int main(int argc, char** argv) {
    USE(argc);
    USE(argv);

    gst_init (&argc, &argv);

    GMainLoop *main_loop;
    main_loop = g_main_loop_new (NULL, FALSE);
    ostringstream launch_stream;

    launch_stream
    << "nvarguscamerasrc name=mysource ! "
    << "video/x-raw(memory:NVMM),width="<< w <<",height="<< h <<",framerate=15/1,format=NV12 ! "
    << "nvv4l2h264enc ! h264parse ! qtmux ! "
    << "filesink location=a.mp4 ";

    launch_string = launch_stream.str();

    g_print("Using launch string: %s\n", launch_string.c_str());

    GError *error = NULL;
    gst_pipeline  = (GstPipeline*) gst_parse_launch(launch_string.c_str(), &error);

    if (gst_pipeline == NULL) {
        g_print( "Failed to parse launch: %s\n", error->message);
        return -1;
    }
    if(error) g_error_free(error);

    

    g_print("attaching probe\n");
    GstElement* src = gst_bin_get_by_name(GST_BIN(gst_pipeline), "mysource");
    GstPad *src_pad = gst_element_get_static_pad (src, "src");
    gst_pad_add_probe (src_pad, GST_PAD_PROBE_TYPE_BUFFER,
    nvargus_src_pad_buffer_probe, NULL, NULL);

    gst_element_set_state((GstElement*)gst_pipeline, GST_STATE_PLAYING);
    
    g_usleep(30*usec);
    g_print("after sleep\n");
    gst_element_send_event (src, gst_event_new_eos ());
    // Wait for EOS message
    //GstBus *bus = gst_pipeline_get_bus(GST_PIPELINE(gst_pipeline));
    //gst_bus_poll(bus, GST_MESSAGE_EOS, GST_CLOCK_TIME_NONE);

    gst_element_set_state((GstElement*)gst_pipeline, GST_STATE_NULL);
    gst_object_unref(GST_OBJECT(gst_pipeline));
    g_main_loop_unref(main_loop);

    g_print("going to exit \n");
    return 0;
}


