/**
 * @file deepstream_caption_stamper_node.cpp
 * @author Bill Drozd (bdrozd@rec.ri.cmu.edu)
 * @brief This node is designed to act a as a sink for GStreamer/Deepstream output.
 * 
 * It provides the following capabilities:
 * 
 * 1) Forward GStreamer Metadata to ROS topics.
 * 
 * 2) Store video frames read from a GStreamer sink into a file, with an added subtitle
 * track providing timestamps for each frame. It is expected that there is a one-to-one correspondance
 * between subtitle entries and video frames. 
 * 
 * Any lack of consistency indicates a error.
 * 
 * @version 0.1
 * @date 2020-11-10
 * 
 * 
 * 
 * @copyright CMU-RI AirLAB ACopyright (c) 2020
 * 
 */
#include <deepstream_ros/deepstream_caption_stamper_node.h>

/// @note Just keep these boiler-plate defines in for now

/// For drawing text on images
#define MAX_DISPLAY_LEN 64

/* The muxer output resolution must be set if the input streams will be of
 * different resolution. The muxer will scale all the input frames to this
 * resolution. */
#define MUXER_OUTPUT_WIDTH 1920
#define MUXER_OUTPUT_HEIGHT 1080

/* Muxer batch formation timeout, for e.g. 40 millisec. Should ideally be set
 * based on the fastest source's framerate. */
#define MUXER_BATCH_TIMEOUT_USEC 40000

/* NVIDIA Decoder source pad memory feature. This feature signifies that source
 * pads having this capability will push GstBuffers containing NvBufSurface. */
#define MEMORY_FEATURES "memory:NVMM"

#define USE(x) ((void)(x))

gint frame_number = 0;

GstClockTime usec = 1000000;
  
//TimeSync::SharedMemoryInterface shminterface;
//TimeSync::Data shmdata;

/*static GQuark gst_buffer_metadata_quark = 0;

typedef struct AuxBufferData {
  gint64 frame_num;
  gint64 timestamp;
  } AuxData;*/

TimeSync::SharedMemoryInterface shminterface;
TimeSync::Data shmdata;

static GQuark gst_buffer_metadata_quark = 0;

typedef struct AuxBufferData {
  gint64 frame_num;
  gint64 timestamp;
} AuxData;

static void convertTimeToTimeSpec(const gint64 input, struct timespec &converted)
{
  converted.tv_sec = input/1000000000;
  gint64 nsecsleft = input - converted.tv_sec * 1000000000;
  converted.tv_nsec = nsecsleft;
}

static GstElement* src_for_signal;

/* osd_sink_pad_buffer_probe  will extract metadata received on OSD sink pad
 * and update params for drawing rectangle, object information etc. */

static gboolean
bus_call(GstBus *bus, GstMessage *msg, gpointer data)
{
  GMainLoop *loop = (GMainLoop *)data;
  switch (GST_MESSAGE_TYPE(msg))
  {
  case GST_MESSAGE_EOS:
    g_print("End of stream\n");
    g_main_loop_quit(loop);
    break;
  case GST_MESSAGE_ERROR:
  {
    gchar *debug;
    GError *error;
    gst_message_parse_error(msg, &error, &debug);
    g_printerr("ERROR from element %s: %s\n",
               GST_OBJECT_NAME(msg->src), error->message);
    if (debug)
      g_printerr("Error details: %s\n", debug);
    g_free(debug);
    g_error_free(error);
    g_main_loop_quit(loop);
    break;
  }
  default:
    break;
  }
  return TRUE;
}

static GstPadProbeReturn nvargus_src_pad_buffer_probe (GstPad * pad, GstPadProbeInfo * info,   gpointer u_data)
{
  shminterface.getData(shmdata);
  if(info)
    { 
      GstBuffer *buf = (GstBuffer *) info->data;
      ROSStreamSink* rosData = (ROSStreamSink*) u_data;

      if (rosData != NULL){
	rosData->argus_source_pad_buffer_probe(pad, info, u_data);
      } 
      
      
    }
  else
    printf("info null\n");

  return GST_PAD_PROBE_OK;
}



static GstPadProbeReturn
osd_sink_pad_buffer_probe(GstPad *pad, GstPadProbeInfo *info,
                          gpointer u_data)
{
  GstBuffer *buf = (GstBuffer *)info->data;
  //shminterface.getData(shmdata);
  //AuxData *meta = NULL;
  
  ROSStreamSink* rosData = (ROSStreamSink*) u_data;

  if (rosData != NULL){
    rosData->ros_sink_pad_buffer_probe(pad, info, u_data);
  } 

  guint num_rects = 0;
  NvDsObjectMeta *obj_meta = NULL;
  guint vehicle_count = 0;
  guint person_count = 0;
  NvDsMetaList *l_frame = NULL;
  NvDsMetaList *l_obj = NULL;
  NvDsDisplayMeta *display_meta = NULL;

  NvDsBatchMeta *batch_meta = gst_buffer_get_nvds_batch_meta(buf);

  GstClock *clock = gst_system_clock_obtain();
  ros::Time now = ros::Time::now();
  GstClockTime ct = gst_clock_get_time(clock);
  gst_object_unref(clock);
  //time_offset_ = now.toSec() - GST_TIME_AS_USECONDS(ct) / 1e6;
  //ROS_INFO("Time offset: %.3f", time_offset_);

  //buildROSObjectMeta(batch_meta);

  if (batch_meta != NULL){
  
  for (l_frame = batch_meta->frame_meta_list; l_frame != NULL;
       l_frame = l_frame->next)
  {
    NvDsFrameMeta *frame_meta = (NvDsFrameMeta *)(l_frame->data);
    int offset = 0;
    for (l_obj = frame_meta->obj_meta_list; l_obj != NULL; l_obj = l_obj->next)
    {
      obj_meta = (NvDsObjectMeta *)(l_obj->data);

      /*if (obj_meta->class_id == PGIE_CLASS_ID_VEHICLE)
      {
        vehicle_count++;
        num_rects++;
      }
      if (obj_meta->class_id == PGIE_CLASS_ID_PERSON)
      {
        person_count++;
        num_rects++;
      } */
    }

    display_meta = nvds_acquire_display_meta_from_pool(batch_meta);
    NvOSD_TextParams *txt_params = &display_meta->text_params[0];
    display_meta->num_labels = 1;
    txt_params->display_text = (char *)g_malloc0(MAX_DISPLAY_LEN);
    guint hours, mins, secs, msecs;

    hours = (guint)(ct / (GST_SECOND * 60 * 60));
    mins = (guint)((ct / (GST_SECOND * 60)) % 60);
    secs = (guint)((ct / GST_SECOND) % 60);
    msecs = (guint)((ct % GST_SECOND) / (1000 * 1000));
    //ROS_INFO("%u:%02u:%02u,%03u", hours, mins, secs, msecs);

    /*
    offset =
        snprintf(txt_params->display_text, MAX_DISPLAY_LEN, "Person = %d ",
                 person_count);
    offset =
        snprintf(txt_params->display_text + offset, MAX_DISPLAY_LEN,
                 "Vehicle = %d ", vehicle_count);
   */
    offset =
        snprintf(txt_params->display_text + offset, MAX_DISPLAY_LEN,
                 "%u:%02u:%02u,%03u", hours, mins, secs, msecs);
    /* Now set the offsets where the string should appear */
    txt_params->x_offset = 10;
    txt_params->y_offset = 12;

    /* Font , font-color and font-size */
    txt_params->font_params.font_name = "Serif";
    txt_params->font_params.font_size = 10;
    txt_params->font_params.font_color.red = 1.0;
    txt_params->font_params.font_color.green = 1.0;
    txt_params->font_params.font_color.blue = 1.0;
    txt_params->font_params.font_color.alpha = 1.0;

    /* Text background color */
    txt_params->set_bg_clr = 1;
    txt_params->text_bg_clr.red = 0.0;
    txt_params->text_bg_clr.green = 0.0;
    txt_params->text_bg_clr.blue = 0.0;
    txt_params->text_bg_clr.alpha = 1.0;

    nvds_add_display_meta_to_frame(frame_meta, display_meta);
  }
  }
  //g_print("Frame Number = %d Number of objects = %d "
  //       "Vehicle Count = %d Person Count = %d\n",
  //        frame_number, num_rects, vehicle_count, person_count);

  frame_number++;
  return GST_PAD_PROBE_OK;
}



void gstreamSigintHandler(int sig)
{
  // Do some custom action.
  // For example, publish a stop message to some other nodes.

  if (src_for_signal != NULL){
    gst_element_send_event (src_for_signal, gst_event_new_eos ());
    ROS_WARN_STREAM("Sent EOS!");
  } 

  
  
  // All the default sigint handler does is call shutdown()
  //ros::shutdown();
}




int main(int argc, char **argv)
{
  USE(argc);
  USE(argv);

  gst_init(&argc, &argv);
  GMainLoop *main_loop;
  main_loop = g_main_loop_new (NULL, FALSE);
  GstBus *bus = NULL;
  guint bus_watch_id;


  ros::init(argc, argv, "deepstream_caption_stamper_node");
  ros::NodeHandle nh_;
  ros::NodeHandle nh_private_("~");


  // Override the default ros sigint handler.
  // This must be set after the first NodeHandle is created.
  signal(SIGINT, gstreamSigintHandler);
  
  std::string gsconfig_rosparam;
  bool gsconfig_rosparam_defined = nh_private_.getParam("gstream_config", gsconfig_rosparam);

  if (!gsconfig_rosparam_defined)
  {
    ROS_FATAL("Parameter gstream_config must be defined to launch this node. This should contain a string with a valid GStreamer Pipeline.");
    return -1;
  }

  ROS_INFO_STREAM(gsconfig_rosparam);

  std::string camera_id_rosparam;
  bool camera_id_rosparam_defined = nh_private_.getParam("cam", camera_id_rosparam);

  if (!camera_id_rosparam_defined)
  {
    ROS_INFO_STREAM(camera_id_rosparam);
    ROS_FATAL("Parameter camera_id  must be defined to launch this node. ");
    return -1;
  }

  std::string srt_file_rosparam;
  bool srt_rosparam_defined = nh_private_.getParam("temp_srt", srt_file_rosparam);
  
  std::string video_file_rosparam;
  bool video_rosparam_defined = nh_private_.getParam("temp_vid", video_file_rosparam);

  std::string data_path_rosparam;
  bool data_path_rosparam_defined = nh_private_.getParam("output_path", data_path_rosparam);
  
  float camera_record_in_secs;
  bool camera_record_rosparam_defined = nh_private_.getParam("record_time", camera_record_in_secs);
  ROS_WARN_STREAM("RECORDING for: " << camera_record_in_secs << " seconds");
 
  ROS_INFO_STREAM("CAMERA="<<camera_id_rosparam << " " << srt_file_rosparam << " " << video_file_rosparam);
  if (!srt_rosparam_defined || !video_rosparam_defined){
    return -1;
  }

  std::string start_time_rosparam;
  bool start_time_rosparam_defined = nh_private_.getParam("start_time", start_time_rosparam);
  replace_string(start_time_rosparam, "\n", "");

  replace_string(gsconfig_rosparam, "START", start_time_rosparam);
  replace_string(srt_file_rosparam, "START", start_time_rosparam);
  replace_string(video_file_rosparam, "START", start_time_rosparam);
  
  ROS_ERROR_STREAM("GSTEARM " << gsconfig_rosparam);
  
  
  
  //ROSStreamSink rss(nh, nh_private, NULL, NULL);
  GError *error = NULL;

  GstPipeline *gst_pipeline  = (GstPipeline *) gst_parse_launch(gsconfig_rosparam.c_str(), &error);
  /* we add a message handler */
  bus = gst_pipeline_get_bus(GST_PIPELINE(gst_pipeline));
  bus_watch_id = gst_bus_add_watch(bus, bus_call, main_loop);
  gst_object_unref(bus);

  //GstElement *src = gst_bin_get_by_name(GST_BIN(gst_pipeline), "mysink");
  GstElement *src = gst_bin_get_by_name(GST_BIN(gst_pipeline), "mysource");

  if(error) g_error_free(error);
  
  if (src==NULL){
    ROS_ERROR_STREAM("Failed to get sink!");
  }
  
  //GstPad *src_pad = gst_element_get_static_pad(src, "sink");
  GstPad *src_pad = gst_element_get_static_pad(src, "src");
   
  if (src_pad==NULL){
    ROS_ERROR_STREAM("Failed to get sink PAD!");
  }

  ROSStreamSink rss(nh_, nh_private_, camera_id_rosparam, srt_file_rosparam, video_file_rosparam, data_path_rosparam, src_pad, (GstElement*) gst_pipeline);

  gst_pad_add_probe(src_pad, GST_PAD_PROBE_TYPE_BUFFER,
                    nvargus_src_pad_buffer_probe, &rss, NULL);
  gst_object_unref(src_pad);

  gst_element_set_state((GstElement *)gst_pipeline, GST_STATE_PLAYING);

  /* Wait till pipeline encounters an error or EOS */
  g_print("Running...\n");
  src_for_signal = src;
  
  if (true){
    g_main_loop_run(main_loop);
  } else {  
    g_usleep(camera_record_in_secs*usec);
  }

  ROS_WARN_STREAM("Exiting Main Loop!");

  //g_print("after sleep\n");
  //
  // Wait for EOS message
  //bus = gst_pipeline_get_bus(GST_PIPELINE(gst_pipeline));
  //gst_bus_poll(bus, GST_MESSAGE_EOS, GST_CLOCK_TIME_NONE);


  /* Out of the main loop, clean up nicely */
  g_print("Returned, stopping playback\n");
  gst_element_set_state((GstElement*) gst_pipeline, GST_STATE_NULL);
  g_print("Deleting pipeline\n");
  gst_object_unref(GST_OBJECT(gst_pipeline));
  g_source_remove(bus_watch_id);
  g_main_loop_unref(main_loop);
  rss.close_stream();
}
