#ifndef GSTREAM_ROS_CAPTION_STAMPER_H
#define GSTREAM_ROS_CAPTION_STAMPER_H

/**
 * @file deepstream_caption_stamper_node.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2020-11-11
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include <ros/ros.h>
#include <gst/gst.h>
#include <glib.h>
#include <stdio.h>
#include <fstream>
#include "gstnvdsmeta.h"
#include <string.h>
#include <chrono>  // chrono::system_clock
#include <ctime>   // localtime
#include <sstream> // stringstream
#include <iomanip> // put_time
#include <string>  // string

#include "std_msgs/String.h"
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/SetCameraInfo.h>
#include <sensor_msgs/image_encodings.h>
#include <deepstream_ros/NvDsObjectMeta_ros.h>
#include <deepstream_ros/NvDsFrameMeta_ros.h>
#include <deepstream_ros/argus_time.h>
#include <deepstream_ros/serialization_util.h>

#include <boost/make_shared.hpp>

// For timekeeping
#include <time.h>
#include <string>
#include <memory>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>


static bool replace_string(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}


class ROSStreamSink
{

public:
 ROSStreamSink(ros::NodeHandle& nh_, ros::NodeHandle& nh_private_, std::string& _camera_id, std::string& _temp_srt, std::string& _temp_vid, std::string& _data_path, GstPad *sink_pad, GstElement *pipeline_) : nh(nh_), nh_private(nh_private_),  srt_file(_temp_srt), video_file(_temp_vid), camera_id(_camera_id), data_path(_data_path)
    {
      ROS_INFO_STREAM("Camera ID: " << camera_id);

        last_time = "00:00:00,000";
        frame_pub_ = nh.advertise<deepstream_ros::NvDsFrameMeta_ros>("ros_deep_frame", 1000);
	time_pub_ = nh.advertise<std_msgs::String>("ros_deep_frame_time/camera_"+camera_id, 1000);

	frame_count = 1;

        pipeline = pipeline_;

        if (sink_pad)
        {
            // gst_pad_add_probe(sink_pad, GST_PAD_PROBE_TYPE_BUFFER,
            //                  &ROSStreamSink::ros_sink_pad_buffer_probe, this, NULL);
        }
        else
        {
            ROS_ERROR_STREAM("Failed to attach probe to pad! -- the pad was NULL");
        }

	        // Get additional configuration parameters
        //nh_private.param("sync_sink", sync_sink);
        //nh_private.param("preroll", preroll);
        //nh_private.param("use_gst_timestamps", use_gst_timestamps, false);

    }
  
  static void convertTimeToTimeSpec(const gint64 input, struct timespec &converted)
  {
    converted.tv_sec = input/1000000000;
    gint64 nsecsleft = input - converted.tv_sec * 1000000000;
    converted.tv_nsec = nsecsleft;
  }
  
  

    void close_stream()
    {
        std::ofstream outfile;
        outfile.open(srt_file); // append instead of overwrite
        outfile << srt_stream.rdbuf();
        outfile.close();
        ROS_INFO_STREAM("DONE Write SRT!");

        buildFFMpegCmd();
        ROS_INFO_STREAM("DONE Write SRT!+MP4");
    }

    bool file_exists(const std::string &name)
    {
        struct stat buffer;
        return (stat(name.c_str(), &buffer) == 0);
    }

    /**
     * @brief 
     * 
     * @param pad 
     * @param info 
     * @param u_data 
     * @return GstPadProbeReturn 
     */
    GstPadProbeReturn ros_sink_pad_buffer_probe(GstPad *pad, GstPadProbeInfo *info, gpointer u_data)
    {
        GstBuffer *buf = (GstBuffer *)info->data;
	
        if (buf)
        {
            NvDsBatchMeta *batch_meta = gst_buffer_get_nvds_batch_meta(buf);
	    if (batch_meta != NULL){
	      frame_pub_.publish(SerializationUtil::convertMsg(batch_meta));
	    }
            //std::string nextTime = getTimestamp(buf);
            //ROS_INFO_STREAM("Appending Stream! " << nextTime);
            //srt_stream << nextTime;
        }
        else
        {
            /// NOP -- but is it bad??
        }
        return GST_PAD_PROBE_OK;
    }

    GstPadProbeReturn argus_source_pad_buffer_probe(GstPad *pad, GstPadProbeInfo *info, gpointer u_data)
    {
        GstBuffer *buf = (GstBuffer *)info->data;
	
        if (buf)
        {      
            std::string nextTime = getTimestamp(buf);
            ROS_INFO_STREAM("Appending Stream! " << nextTime);
            srt_stream << nextTime;
	    std_msgs::String msg;
	    msg.data = nextTime;
	    time_pub_.publish(msg);
        }
        else
        {
            /// NOP -- but is it bad??
        }
        return GST_PAD_PROBE_OK;
    }


    /// Use this to write the SRT Stream during execution
    std::stringstream srt_stream;
    bool gst_is_initialized;
    uint32_t frame_count;
    std::string gsconfig;

    std::string last_time;
    ros::Publisher frame_pub_;
    ros::Publisher time_pub_;
    GstClockTime start_time;
    GstElement *pipeline;
    GstElement *sink;

private:


    void debugParams(){
      std::vector<std::string> keys;
      nh.getParamNames(keys);
	for (auto& pname : keys){
	  ROS_INFO_STREAM("PAram:" << pname);
	}
     }
      
    
    /**
     * @brief Execute FFMPEG command to insert subtitles into video file....just a bit more flexible
     * 
     * @param video_file 
     * @param srt_file 
     * @param output_file 
     * @return std::string 
     */
    void buildFFMpegCmd()
    {

      //debugParams();
        auto now = std::chrono::system_clock::now();
        auto in_time_t = std::chrono::system_clock::to_time_t(now);

        std::string file_prefix;
        nh.getParam("OUTPUT_FILENAME", file_prefix);

        std::stringstream ss;
	
        //ss << data_path << camera_id << "_" << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d-%H-%M-%S") << ".mkv";
	
	std::string output_code = video_file;
	replace_string(output_code, "sink", "captioned");
	ss << output_code;
	

        if (!file_exists(video_file) || !file_exists(srt_file))
        {
            ROS_ERROR_STREAM("Cannot find output video file: " << video_file << " or srt_file: " << srt_file);
        }

        std::stringstream ffMPEGStream;
        ffMPEGStream << "ffmpeg -y -hide_banner -loglevel panic -i " << video_file << " -f srt -i " << srt_file << " -c copy -c:s srt " << ss.str();
        //ffMPEGStream << " -map 0:0 -map 0:1 -map 1:0 -c:v copy -c:a copy -c:s mov_text " << output_file;
        ROS_WARN_STREAM("Building Caption File Please Do Not Terminate!" << ffMPEGStream.str());
        std::system(ffMPEGStream.str().c_str());
    }

    void printClock(GstClockTime &ct)
    {
        guint hours, mins, secs, msecs;

        hours = (guint)(ct / (GST_SECOND * 60 * 60));
        mins = (guint)((ct / (GST_SECOND * 60)) % 60);
        secs = (guint)((ct / GST_SECOND) % 60);
        msecs = (guint)((ct % GST_SECOND) / (1000 * 1000));
        ROS_INFO("%u:%02u:%02u,%03u", hours, mins, secs, msecs);
    }

    gchar *createSRTStamp()
    {
        /*GstClock *clock = gst_system_clock_obtain();
        GstClockTime ct = gst_clock_get_time(clock);
        gst_object_unref(clock);

        GstClockTime bt = gst_element_get_base_time(pipeline);
        ct -= start_time;
        printClock(ct);*/
        guint hours, mins, secs, msecs;

        gint64 current = -1;

        /* Query the current position of the stream */
        if (!gst_element_query_position(pipeline, GST_FORMAT_TIME, &current))
        {
            g_printerr("Could not query current position.\n");
        }

        hours = (guint)(current / (GST_SECOND * 60 * 60));
        mins = (guint)((current / (GST_SECOND * 60)) % 60);
        secs = (guint)((current / GST_SECOND) % 60);
        msecs = (guint)((current % GST_SECOND) / (1000 * 1000));
        ROS_INFO("THIS ONE %u:%02u:%02u,%03u", hours, mins, secs, msecs);
       
       
       return g_strdup_printf("%u:%02u:%02u,%03u", hours, mins, secs, msecs);
       
       
       /* if (!GST_CLOCK_TIME_IS_VALID(ct))
        {
            ROS_ERROR_STREAM("INVALID CLOCK Time");
            return g_strdup("");
        }

        hours = (guint)(ct / (GST_SECOND * 60 * 60));
        mins = (guint)((ct / (GST_SECOND * 60)) % 60);
        secs = (guint)((ct / GST_SECOND) % 60);
        msecs = (guint)((ct % GST_SECOND) / (1000 * 1000));
        ROS_INFO("%u:%02u:%02u,%03u", hours, mins, secs, msecs);
        printClock(bt);
       */
    }

    /*
    static std::string createSRTStamp(timespec &tv)
    {

        ROS_INFO_STREAM("H!");
        char *buffer = "00:00:00,000";
        ROS_INFO_STREAM("H!");

        uint32_t hours = floor(tv.tv_sec % 3600);
        uint32_t mins = floor(tv.tv_sec % 60);
        uint32_t secs = floor(tv.tv_sec);
        uint32_t millis = floor(tv.tv_nsec * 1e-6);
        ROS_INFO_STREAM(hours << ":" << mins << ":" << secs << "," << millis);
        sprintf(buffer,"%.2d:%.2d:%.2d,%.3d", hours, mins, secs, millis);
        ROS_INFO_STREAM("H!");

        return std::string(buffer);
    } */

    std::string getTimestamp(GstBuffer *buf)
    {

        argus::AuxData *meta = NULL;
        std::stringstream srt_entry_stream;

        if (buf)
        {
            srt_entry_stream << frame_count << std::endl;
	    argus::shminterface.getData(argus::shmdata);
	    if (!argus::shmdata.valid){
	      ROS_ERROR_STREAM("SHM INVALID!");
	    }

            argus::gst_buffer_metadata_quark = g_quark_from_static_string("GstBufferMetaData");
            meta = (argus::AuxData *)gst_mini_object_get_qdata(GST_MINI_OBJECT_CAST(buf), argus::gst_buffer_metadata_quark);
            struct timespec systemtime, monotonic, monotonicimage, time;
            clock_gettime(CLOCK_REALTIME, &systemtime);
            ROS_INFO("RT %lu\t%lu\n", systemtime.tv_sec, systemtime.tv_nsec);
            clock_gettime(CLOCK_MONOTONIC, &monotonic);
            ROS_INFO("MT %lu\t%lu\n", monotonic.tv_sec, monotonic.tv_nsec);

            std::string srt_stamp = createSRTStamp();

            if (srt_stamp.compare(last_time) == 0)
            {
                ROS_WARN_STREAM("Duplicate time? " << srt_stamp);
            } else {
                ROS_INFO_STREAM(last_time << " --> " << srt_stamp);
            } 
            srt_entry_stream << last_time << " --> " << srt_stamp << std::endl;

            last_time = srt_stamp;
            frame_count++;

	    
            ///Now write the content into the SRT....a bunch of time stamps!
            srt_entry_stream << "RT: " << systemtime.tv_sec << "    " << systemtime.tv_nsec << std::endl;
            srt_entry_stream << "MT: " << monotonic.tv_sec << "    " << monotonic.tv_nsec << std::endl;
            ROS_INFO_STREAM("Start: " << GST_PIPELINE(pipeline)->stream_time);

            if (meta != NULL)
            {
                ROS_INFO(">>> Gstreamer:Frame #%lu : Timestamp: %lu\n", meta->frame_num, meta->timestamp);

		convertTimeToTimeSpec(meta->timestamp, monotonicimage);
		ROS_INFO("MTI %lu\t%lu\n", monotonicimage.tv_sec, monotonicimage.tv_nsec);
                srt_entry_stream << "MTI: " << monotonicimage.tv_sec << "    " << monotonicimage.tv_nsec << std::endl;
                if (TimeSync::convertToSystemTimeMono(monotonicimage, argus::shmdata, time))
                {
                    ROS_INFO("tc: %ld %ld\n", time.tv_sec, time.tv_nsec);
                    ROS_INFO("monotonic diff: %ld ns\n", systemtime.tv_nsec - time.tv_nsec);
                    srt_entry_stream << "tc: " << time.tv_sec << "    " << time.tv_nsec << std::endl;
                    srt_entry_stream << "monotonic diff: " << systemtime.tv_nsec - time.tv_nsec << std::endl;
                }
                else{
		  ROS_WARN_STREAM("TimeSync Shared Memory failure!");
		}
            }
        }
        else
        {
            ROS_ERROR_STREAM("Can't get timestamp with bad buffer!");
        }
        srt_entry_stream << std::endl;
        
        return srt_entry_stream.str();
    }
    std::string camera_id;
    std::string srt_file;
    std::string video_file;
    std::string data_path;
    ros::NodeHandle nh, nh_private;

    bool sync_sink;
    bool preroll;
    bool use_gst_timestamps;

    // ROS Inteface
    // Calibration between ros::Time and gst timestamps
    double time_offset_;

    
    //TimeSync::SharedMemoryInterface shminterface;
    //TimeSync::Data shmdata;

};

// namespace castacks
#endif
