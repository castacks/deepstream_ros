#ifndef __ARGUSTIMEDATA_H_
#define __ARGUSTIMEDATA_H_

#define _GLIBCXX_USE_CXX11_ABI 1
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <gst/gst.h>
#include <stdio.h>
#include <time.h>
#include <timesyncdata.h>

namespace argus
{

TimeSync::SharedMemoryInterface shminterface;
TimeSync::Data shmdata;

static GQuark gst_buffer_metadata_quark = 0;

typedef struct AuxBufferData
{
    gint64 frame_num;
    gint64 timestamp;
} AuxData;

static void convertTimeToTimeSpec(const gint64 input, struct timespec &converted)
{
    converted.tv_sec = input / 1000000000;
    gint64 nsecsleft = input - converted.tv_sec * 1000000000;
    converted.tv_nsec = nsecsleft;
}

static GstPadProbeReturn nvargus_src_pad_buffer_probe(GstPad *pad, GstPadProbeInfo *info, gpointer u_data)
{
    shminterface.getData(shmdata);
    AuxData *meta = NULL;
    if (info)
    {
        GstBuffer *buf = (GstBuffer *)info->data;
        if (buf)
        {
            gst_buffer_metadata_quark = g_quark_from_static_string("GstBufferMetaData");
            meta = (AuxData *)gst_mini_object_get_qdata(GST_MINI_OBJECT_CAST(buf), gst_buffer_metadata_quark);
            if (meta)
            {
                printf(">>> Gstreamer:Frame #%lu : Timestamp: %lu\n", meta->frame_num, meta->timestamp);
                struct timespec systemtime, monotonic, monotonicimage, time;
                clock_gettime(CLOCK_REALTIME, &systemtime);
                printf("RT %lu\t%lu\n", systemtime.tv_sec, systemtime.tv_nsec);
                clock_gettime(CLOCK_MONOTONIC, &monotonic);
                printf("MT %lu\t%lu\n", monotonic.tv_sec, monotonic.tv_nsec);
                convertTimeToTimeSpec(meta->timestamp, monotonicimage);
                printf("MTI %lu\t%lu\n", monotonicimage.tv_sec, monotonicimage.tv_nsec);
                if (convertToSystemTimeMono(monotonicimage, shmdata, time))
                {
                    printf("tc: %ld %ld\n", time.tv_sec, time.tv_nsec);
                    printf("monotonic diff: %ld ns\n", systemtime.tv_nsec - time.tv_nsec);
                }
                else
                    printf("no init\n");
            }
            else
                printf("meta null\n");
        }
        else
            printf("buf null\n");
    }
    else
        printf("info null\n");
    return GST_PAD_PROBE_OK;
}

} // namespace argus
#endif