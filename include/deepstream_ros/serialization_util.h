#ifndef GSTREAM_ROS_SERIALIZE_H
#define GSTREAM_ROS_SERIALIZE_H
#include <deepstream_ros/NvDsObjectMeta_ros.h>
#include <deepstream_ros/NvDsFrameMeta_ros.h>

/**
 * @brief This is a class used to map data between ROS <---> GStreamer types. 
 * 
 */
class SerializationUtil
{

public:
    static deepstream_ros::NvDsFrameMeta_ros::Ptr convertMsg(NvDsBatchMeta *batch_meta)
    {

        if (batch_meta == NULL)
        {
            throw std::invalid_argument("Convert Message from NvDsBatchMeta to ROS was given a null pointer!");
        }

        /// Allocate the ROS container
        deepstream_ros::NvDsFrameMeta_ros::Ptr rosFrame = boost::make_shared<deepstream_ros::NvDsFrameMeta_ros>();

        /// TODO: Fix this to a better time provider....
        rosFrame->header.stamp = ros::Time::now();

        /// Loop through every frame in the Batch
        for (NvDsMetaList *l_frame = batch_meta->frame_meta_list; l_frame != NULL;
             l_frame = l_frame->next)
        {

            NvDsFrameMeta *frame_meta = (NvDsFrameMeta *)(l_frame->data);
            int offset = 0;

            rosFrame->gstream_clock = frame_meta->ntp_timestamp;

            for (NvDsMetaList *l_obj = frame_meta->obj_meta_list; l_obj != NULL; l_obj = l_obj->next)
            {
                NvDsObjectMeta *obj_meta = (NvDsObjectMeta *)(l_obj->data);

                //Move this somewhere better.....

                deepstream_ros::NvDsObjectMeta_ros objROS;
                objROS.unique_component_id = obj_meta->unique_component_id;
                objROS.class_id = obj_meta->class_id;
                objROS.object_id = obj_meta->object_id;
                objROS.confidence = obj_meta->confidence;
                std::stringstream sstr;
                sstr << (char *)obj_meta->obj_label;
                objROS.obj_label = sstr.str();

                objROS.left = GST_ROUND_UP_2((unsigned int)obj_meta->rect_params.left);
                objROS.top = GST_ROUND_UP_2((unsigned int)obj_meta->rect_params.top);
                objROS.width = GST_ROUND_DOWN_2((unsigned int)obj_meta->rect_params.width);
                objROS.height = GST_ROUND_DOWN_2((unsigned int)obj_meta->rect_params.height);

                rosFrame->allObjects.push_back(objROS);
            }
        }
        return rosFrame;
    }
};

#endif